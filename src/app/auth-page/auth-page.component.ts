import { Component } from '@angular/core';


@Component({
    selector:'auth-page',
    styleUrls: ['./auth-page.component.scss'],
    templateUrl: './auth-page.component.html'
})
export class AuthPageComponent{

    signIn:boolean = true;
    signUp:boolean = false;

    changeTab(event){
        let elem:string =  event.attributes.id.nodeValue
        if(elem === 'signin'){
            this.signIn = true;
            this.signUp = false;
        }
        else if(elem === 'signup'){
            this.signIn = false;
            this.signUp = true;
        }
    }


}